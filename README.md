# skycoin-mobile

A simple mobile application with go-mobile. Start a web server and accessible on local host from the mobile device

## Installation

### Install Android SDK/NDK

* Install Android-SDK with Android Studio or Stand-alone SDK tools follow official guide
```
http://www.androiddocs.com/sdk/installing/index.html
```

* Intall Android-NDK https://developer.android.com/ndk/guides/

* Config environment variables

```bash
$ANDROID_SDK = "path_to_android_sdk"
$ANDROID_NDK = "path_to_android_ndk"
```

### Install ___gomobile___ tools


Get [gomobile](https://godoc.org/golang.org/x/mobile/cmd/gomobile)

```bash
$ go get golang.org/x/mobile/cmd/gomobile
```

Initialize gomobile with android-ndk

```bash
$ gomobile init -ndk $ANDROID_NDK
```

Build the .apk file

```bash
$ gomobile build --target=android -o=$GOPATH\src\go-mobile-app-test\SkycoinMobile.apk go-mobile-app-test
```

_Note: Can install the app directly from terminal with following command_

```bash
$ gomobile install skycoin\skycoin-mobile
```

## Usage

After install the app to android mobile device. Open the app on the phone then access `http://localhost:8080`
on phone's browser