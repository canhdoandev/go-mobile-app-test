package lib

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPostLocation(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := map[string]string{}
		err := json.NewDecoder(r.Body).Decode(&body)
		r.Body.Close()

		require.Nil(t, err)
		assert.Equal(t, "-10.00001", body["latitude"])
		assert.Equal(t, "100.124578", body["longitude"])
		assert.Equal(t, "application/json", r.Header.Get("Content-Type"))

		fmt.Fprintln(w, "OK")
	}))
	defer ts.Close()

	client := &Client{ts.URL}
	err := client.PostLocation("-10.00001", "100.124578")
	require.Nil(t, err)
}

func TestPostLocationFailed(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "foo", http.StatusForbidden)
	}))
	defer ts.Close()

	client := &Client{ts.URL}
	err := client.PostLocation("-10.00001", "100.124578")
	require.NotNil(t, err)
	assert.Equal(t, "http POST failed: unexpected status code: 403", err.Error())
}

func TestGenerateKeypair(t *testing.T) {
	f, err := ioutil.TempFile("", "rsa_json")
	require.Nil(t, err)
	defer os.Remove(f.Name())

	err = GenerateKeypair("12345678", f.Name())
	require.Nil(t, err)

	out := map[string]string{}
	err = json.NewDecoder(f).Decode(&out)
	require.Nil(t, err)

	assert.NotNil(t, out["public"])
	assert.NotNil(t, out["private"])
}

func TestGenerateKeypairError(t *testing.T) {
	err := GenerateKeypair("1234", "")
	require.NotNil(t, err)
	assert.Equal(t, "outPath can't be empty", err.Error())

	err = GenerateKeypair("1234", "foo")
	require.NotNil(t, err)
	assert.Equal(t, "invalid pin code length. expected 8. got 4", err.Error())
}
