* gomobile-gps

Sample project with golang library that is can be executed from a
native android app. This repository contains SDK library in ~lib~
folder and native android app that uses it in ~android~ folder.

golang library exposes 1 function ~GenerateKeypair~ and http ~Client~
struct with 1 method ~PostLocation~. Native android app invokes
function that generates keypair on launch, collects current gps
coordinates and posts those to a configured server.

** Installation

To run this app on device or emulator you will need [[https://github.com/golang/go/wiki/Mobile][gomobile]] and
[[https://developer.android.com/studio/][Android Studio]]. Once you installed both simply go get this repository,
open provided project and run it via Android Studio's UI. If ~go~ is
not in accessible path or ~GOPATH~ is not exposed in environment then
adjust values in ~android/lib/build.gradle~ accordingly.

You can use provided HTTP server (in ~cmd~) during testing, it will
start on address ~:8080~ with a single endpoint ~/location~. All json
request to that endpoint will be echoed to stdout. You have to update
url in ~MainActivity.java~ to point to your local web server.
